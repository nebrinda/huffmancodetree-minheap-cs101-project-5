/*
written by Nathan Brinda
11/21/15
cs101-002
project5
*/

#include <iostream>
#include <fstream>
#include <vector>
#include "MinHeap.h"
#include "HeapNode.h"

std::vector<std::string> full_text;

std::vector<int> get_frequencies(char*);

int main(int argc, char** argv)
{
   if (argc != 2)
   {
      std::cerr << "must supply exactly 1 command line argument";
      return 0;
   }

   std::vector<int> letter_frequencies;

   letter_frequencies = get_frequencies(argv[1]);

   MinHeap *heap = new MinHeap();
   heap->make_heap(letter_frequencies);

   heap->build_htree();
   heap->generate_preorder();
   heap->generate_inorder();
   heap->generate_codes();

   std::ofstream fp;
   fp.open("code.txt");

   std::string encoded_letter;
   for (size_t i = 0; i < full_text.size(); i++)
   {
      encoded_letter = heap->encode_letter(full_text[i]);
      fp << encoded_letter;
   }
   heap->print_lengths();
   return 0;
}

//gets the frequencies of the letters and stores them in a vector, even indices represent letters, odd indices represent frequencies 
std::vector<int> get_frequencies(char* filename)
{
   std::fstream file_pointer;
   file_pointer.open(filename);
   std::string strLetter;
   int size = 0;
   int i = 0;
   bool isIn;
   file_pointer.unsetf(std::ios_base::skipws);
   char letter;
   std::vector<int> letter_frequencies;



   while (file_pointer >> letter)
   {
      strLetter += letter;
      full_text.push_back(strLetter);
      strLetter = "";

      isIn = true;
      size = letter_frequencies.size();
      for (i = 0; i < size; i += 2)
      {
         if (letter_frequencies[i] == letter)
         {
            letter_frequencies[i + 1]++;
            isIn = false;
            break;
         }
      }
      if (isIn == true)
      {
         letter_frequencies.push_back((int)letter);
         letter_frequencies.push_back(1);
      }
   }

   file_pointer.close();
   return letter_frequencies;
}