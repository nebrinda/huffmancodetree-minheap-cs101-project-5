/*
written by Nathan Brinda
11/21/15
cs101-002
project5
*/

#pragma once

#include "HeapNode.h"
class InternalNode :
   public HeapNode
{
public:
   InternalNode(int, int, HeapNode*, HeapNode*);

   ~InternalNode();

private:
   int label;

public:
   int get_label();
};

