/*
written by Nathan Brinda
11/21/15
cs101-002
project5
*/

#include "LeafNode.h"

LeafNode::LeafNode(char x, int Freq) :
HeapNode(Freq)
{
   letter = x;
}


LeafNode::~LeafNode()
{
}


int LeafNode::get_ascii()
{
   return int(letter);
}

char LeafNode::get_letter()
{
   return letter;
}