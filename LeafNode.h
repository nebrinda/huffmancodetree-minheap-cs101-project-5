/*
written by Nathan Brinda
11/21/15
cs101-002
project5
*/

#pragma once

#include "HeapNode.h"
class LeafNode :
   public HeapNode
{
public:
   LeafNode(char x, int y);
   ~LeafNode();

private:
   char letter;

public:
   int get_ascii();
   char get_letter();
};

