/*
written by Nathan Brinda
11/21/15
cs101-002
project5
*/

#pragma once

#include <iostream>

class HeapNode
{
public:
   HeapNode(HeapNode*);
   HeapNode(int);

   virtual ~HeapNode();

public:

   HeapNode *parent;
   HeapNode *left;
   HeapNode *right;
   int index;
   int freq;

   HeapNode *ileft;
   HeapNode *iright;
};

