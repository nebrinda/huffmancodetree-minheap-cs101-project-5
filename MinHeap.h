/*
written by Nathan Brinda
11/21/15
cs101-002
project5
*/

#pragma once

#include <iostream>
#include <vector>
#include "HeapNode.h"
#include <fstream>
#include <string>
#include <cmath>


class MinHeap
{
private:

   std::vector<HeapNode*> nodes;
   std::vector<std::string> codes;
   std::ofstream fp;
   std::string codeString;

   bool bubble_min(HeapNode*);
   void insert_node(HeapNode*);
   void bubble_down(HeapNode*);
   bool swap_nodes(HeapNode*, HeapNode*);
   void preorderTraversal(HeapNode*);
   void inorderTraversal(HeapNode*);
   void fill_code_table(HeapNode*, char);


public:
   MinHeap();
   HeapNode* extract_min();
   void make_heap(std::vector<int>);
   int node_count();
   void generate_preorder();
   void generate_inorder();
   void build_htree();
   void generate_codes();
   std::string encode_letter(std::string);
   void print_lengths();
   void print_binary();
};
