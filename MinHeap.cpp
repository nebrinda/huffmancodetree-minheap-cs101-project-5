/*
written by Nathan Brinda
11/21/15
cs101-002
project5
*/

#include "MinHeap.h"
#include "LeafNode.h"
#include "InternalNode.h"

//empty constructor
MinHeap::MinHeap()
{}

//makes the heap
void MinHeap::make_heap(std::vector<int> letter_frequencies)//takes in a vector where all the odd values are letters and even values are their frequencies
{
   for (int i = 0; i < (int)letter_frequencies.size(); i += 2)//creating the min heap
   {
      LeafNode* new_node = new LeafNode(char(letter_frequencies[i]), letter_frequencies[i + 1]);
      insert_node(new_node);
      while (bubble_min(new_node))
         continue;
   }

}

//inserts nodes into the min heap
void MinHeap::insert_node(HeapNode* new_node)
{
   int level_size = 0;
   int level_start = 0;
   int level = -1;
   nodes.push_back(new_node);
   new_node->index = (nodes.size() - 1);
   while (level_start < int(nodes.size()))
   {
      level++;
      level_size = int(pow(2, level));
      level_start += level_size;
   }

   if (level < 1)
      return;

   level_start -= level_size;

   int parent_index = int((nodes.size() - 2)) / 2;
   if (parent_index >= 0)
   {
      new_node->parent = nodes[parent_index];

      if ((nodes.size() - level_start) % 2 == 0)
         nodes[parent_index]->right = new_node;
      else
         nodes[parent_index]->left = new_node;
   }
}

//extracts nodes off the min heap
HeapNode* MinHeap::extract_min()
{
   if (nodes.size() < 2)
   {
      HeapNode* last_node = nodes[0];

      nodes.pop_back();
      return last_node;
   }
   swap_nodes(nodes[0], nodes[nodes.size() - 1]);

   HeapNode* last_node = nodes[nodes.size() - 1];

   if (last_node->parent != NULL)
   {
      if (last_node->parent->left == last_node)
         last_node->parent->left = NULL;
      else if (last_node->parent->right == last_node)
         last_node->parent->right = NULL;

   }
   nodes.pop_back();
   bubble_down(nodes[0]);
   return last_node;

}

//bubbles the root down until the min heap is properly ordered
void MinHeap::bubble_down(HeapNode* current)
{
   if (current->left == NULL && current->right == NULL)
      return;


   if (current->left == NULL)
   {
      if (current->right->freq >= current->freq)
         return;

      swap_nodes(current, current->right);
   }

   else if (current->right == NULL)
   {
      if (current->left->freq >= current->freq)
         return;

      swap_nodes(current, current->left);

   }
   else if (current->left->freq >= current->right->freq)
   {
      if (current->right->freq >= current->freq)
         return;

      swap_nodes(current, current->right);

   }
   else if (current->left->freq < current->right->freq)
   {
      if (current->left->freq >= current->freq)
         return;

      swap_nodes(current, current->left);
   }

   else
      return;

   bubble_down(current);
}

//swaps two nodes
bool MinHeap::swap_nodes(HeapNode* node1, HeapNode* node2)//does not work for extract min
{
   HeapNode *temp1 = node1;

   HeapNode *Copy_node1 = new HeapNode(node1);
   HeapNode *Copy_node2 = new HeapNode(node2);
   bool adjacent = false;

   node1->left = Copy_node2->left;
   node1->right = Copy_node2->right;

   if (node2->parent == node1)
   {
      adjacent = true;
      node1->parent = node2;
   }
   else
      node1->parent = node2->parent;

   if (Copy_node1->left == node2)
   {
      node2->left = temp1;
      node2->right = Copy_node1->right;

      if (node2->right != NULL)
         node2->right->parent = node2;
   }

   else if (Copy_node1->right == node2)
   {
      node2->left = Copy_node1->left;
      node2->right = temp1;

      if (node2->left != NULL)
         node2->left->parent = node2;
   }

   else
   {
      node2->left = Copy_node1->left;
      node2->right = Copy_node1->right;

      if (node2->right != NULL)
         node2->right->parent = node2;

      if (node2->left != NULL)
         node2->left->parent = node2;
   }

   node2->parent = Copy_node1->parent;

   nodes[temp1->index] = node2;
   nodes[node2->index] = temp1;
   temp1->index = Copy_node2->index;
   node2->index = Copy_node1->index;

   if (adjacent)
   {
      if (node2->parent != NULL)
      {
         if (node2->parent->left == temp1)
            node2->parent->left = node2;
         else if (node2->parent->right == temp1)
            node2->parent->right = node2;
      }
   }

   else
   {
      if (temp1->parent != NULL)
      {
         if (temp1->parent->left == node2)
            temp1->parent->left = temp1;
         else if (temp1->parent->right == node2)
            temp1->parent->right = temp1;
      }
   }
   if (temp1->left != NULL)
      temp1->left->parent = temp1;

   if (temp1->right != NULL)
      temp1->right->parent = temp1;

   delete Copy_node1;
   delete Copy_node2;

   return true;
}

//makes sure the smalles value is at the root
bool MinHeap::bubble_min(HeapNode* new_node)
{
   if (NULL == new_node->parent)
      return false;

   if (new_node->parent->freq <= new_node->freq)
      return false;

   return swap_nodes(new_node->parent, new_node);
}

//returns the numbers in the min heap
int MinHeap::node_count()
{
   return nodes.size();
}

//outputs the inorder traversal to a file
void MinHeap::inorderTraversal(HeapNode *current) {
   if (current != NULL)
   {
      inorderTraversal(current->ileft);
      if (dynamic_cast<LeafNode*> (current) != NULL)
         fp << "L:" << ((LeafNode*)current)->get_ascii() << " ";
      else if (dynamic_cast<InternalNode*> (current) != NULL)
         fp << "I:" << ((InternalNode*)current)->get_label() << " ";
      inorderTraversal(current->iright);
   }
}

//outputs the preorder traversal to a file
void MinHeap::preorderTraversal(HeapNode *current) //don't need to pass in file here
{
   if (current != NULL)
   {
      if (dynamic_cast<LeafNode*> (current) != NULL)
         fp << "L:" << ((LeafNode*)current)->get_ascii() << " ";
      else if (dynamic_cast<InternalNode*> (current) != NULL)
         fp << "I:" << ((InternalNode*)current)->get_label() << " ";
      preorderTraversal(current->ileft);
      preorderTraversal(current->iright);
   }
}

//calls preorder traversal
void MinHeap::generate_preorder()
{
   fp.open("tree.txt");
   if (nodes.size() > 0)
      preorderTraversal(nodes[0]);
   fp << std::endl;
}

//calls inorder traversal
void MinHeap::generate_inorder()
{
   if (nodes.size() > 0)
      inorderTraversal(nodes[0]);
   fp.close();
}

//builds the huffman code tree
void MinHeap::build_htree()
{
   int icount = 0;

   while (nodes.size() > 1)
   {
      HeapNode* next_node1 = extract_min();
      HeapNode* next_node2 = extract_min();

      icount++;
      InternalNode* new_node = new InternalNode(icount, next_node1->freq + next_node2->freq,
         next_node1, next_node2);
      insert_node(new_node);
   }
}

//calls fill_code_table
void MinHeap::generate_codes()
{
   fill_code_table(nodes[0], ' ');
}

//creates a table to hold all the encodings and their character equivalences
void MinHeap::fill_code_table(HeapNode* current, char Dir)
{
   if (current != NULL)
   {
      if (Dir == 'l')
         codeString.append("0");
      else if (Dir == 'r')
         codeString.append("1");

      if (dynamic_cast<LeafNode*> (current) != NULL)
      {
         std::string tempstring;
         tempstring += (((LeafNode*)current)->get_letter());
         codes.push_back(tempstring);
         codes.push_back(codeString);
      }

      fill_code_table(current->ileft, 'l');
      fill_code_table(current->iright, 'r');

      if (codeString.size() > 0)
         codeString.pop_back();
   }
}

//looks up a letter in the code table and returns the encoding
std::string MinHeap::encode_letter(std::string input)
{
   for (size_t i = 0; i < codes.size() - 1; i += 2)
   {    
      if (codes[i] == input)
         return codes[i + 1];
   }
   return "";
}

//outputs the length of each encoding to a file
void MinHeap::print_lengths()
{
   std::ofstream fp;
   fp.open("length.txt");

   std::string smallest = "";
   size_t smallest_index = 0;
   while (true)
   {
      smallest = codes[0];
      smallest_index = 0;
      for (size_t i = 0; i < codes.size() - 1; i += 2)
      {
         if (codes[i] < smallest)
         {
            smallest = codes[i];
            smallest_index = i;
         }
      }
      fp << "0" << (int)codes[smallest_index][0] << " " << codes[smallest_index + 1].length() << std::endl;
      codes.erase(codes.begin() + smallest_index);
      codes.erase(codes.begin() + smallest_index);
      if (codes.size() <= 0)
         break;
   }
   fp.close();
}