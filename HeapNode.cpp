/*
written by Nathan Brinda
11/21/15
cs101-002
project5
*/

#include "HeapNode.h"

//constructor
HeapNode::HeapNode(int Freq)
{
   freq = Freq;

   parent = NULL;
   left = NULL;
   right = NULL;
   index = 0;

   ileft = NULL;
   iright = NULL;
}

//copies the values of the node that is passed to it
HeapNode::HeapNode(HeapNode* in)
{
   index = in->index;
   left = in->left;
   right = in->right;
   parent = in->parent;
}

HeapNode::~HeapNode()
{
}
