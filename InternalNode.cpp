/*
written by Nathan Brinda
11/21/15
cs101-002
project5
*/

#include "InternalNode.h"

InternalNode::InternalNode(int l, int f, HeapNode* x , HeapNode* y ) :
HeapNode(f)
{
   label = l;
   ileft = x;
   iright = y;
}

InternalNode::~InternalNode()
{
}

int InternalNode::get_label()
{
   return label;
}